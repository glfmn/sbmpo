# SBMPO

Sampling Based Model Predictive Control (SBMPO), a novel nonlinear MPC (NMPC) approach that enables motion planning with dynamic models as well as the solution of more traditional MPC problems.

The focus of our motion planning research is to develop algorithms that will enable both mobile robots and manipulators to operate intelligently in extreme environments or for extreme tasks. For mobile robots such as autonomous ground vehicles (AGVs), autonomous air vehicles (AAVs), or autonomous underwater vehicles (AUVs) an example of an extreme environment is a cluttered environment. This problem has been addressed using both reactive and deliberative planning algorithms. For AGVs extreme environments also include difficult terrains such as sand, ice and mud, and highly undulating terrains. For manipulators an extreme task is lifting objects that are so heavy that they cannot be lifted quasi-statically. A unifying feature of each of these latter problems is that they benefit from using a dynamic model in the planning process. Hence, a major focus of this research is the development and refinement of SBMPO.

For any more details on the project, please visit our [website](http://www.ciscor.org/motion-planning/).

# Graph Search Vizualization
A webpage that Gwen has provided that has very helpful vizualizations for different graph search methods.
Includes A* method used in our SBMPO implementation:

[Visualizing Graph Search Methods](https://glfmn.github.io/ISC4221/)

# Quick start

Install documentation dependencies:

```
$ pip3 install jinja2 Pygments
```

To build the SBMPO library simply invoke

```
$ cmake -H. -Btarget && cmake --build target
```

To see a full list of supported targets simply invoke

```
$ cmake --build target --target help
```

`make -C [directory]` specifies the directory where your makefile is located.  This allows you to use make as if you were in that directory without changing into it.

This is useful because it allows us to do out of source builds with cmake, which is recommended.  Since cmake generates the best build system for your platform automatically, it's difficult to safely ignore the generated files.  Thus, we build everything in the target directory with `cmake CMakeLists.txt --build target`, and to make this more ergonomic we use `make -C [directory]` assuming make is the current build system.

If you prefer, you can simply enter the build directory and use `make` like normal.


## Automated testing

We have a pre-commit script which automates test execution on every commit, and handles all kinds of nasty edge cases.  If you are on a bash system, you can use it with:

```sh
$ ln -s ../../.pre-commit.sh .git/hooks/pre-commit
```

When you have the hook set up, you can skip tests on a commit by invoking `git commit` with the `--no-verify` option.  You usually want to do this when you want to:

- change only files or lines that aren't code and thus aren't covered by tests
- make a complicated patch which required editing a hunk which would cause merge conflicts on a `git stash apply` (manual testing **encouraged**)

# Dependencies

## Ubuntu 14.04 and lower

SBMPO needs `g++` version 4.9, which you can get by running

```
$ sudo add-apt-repository ppa:ubuntu-toolchain-r/test
$ sudo apt-get update && sudo apt-get install g++-4.9
$ sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-4.9 50
```

This adds the ubuntu toolchain PPA which provides access to newer versions of `g++`, and updates the `g++` command to use `g++-4.9` as desired.

## Documentation

Our documentation system uses [`mcss`] on top of [`doxygen`] which will automatically be downloaded during build system configuration.  It has a few dependencies though:

```
$ pip3 install jinja2 Pygments
```

You can find the documentation at `target/doc/html/index.html` if `target` is the directory you specified with `cmake --build`.

[`mcss`]: http://mcss.mosra.cz/doxygen/
[`doxygen`]: https://www.stack.nl/~dimitri/doxygen/

# See Also

- Test [README](test/README.md)
