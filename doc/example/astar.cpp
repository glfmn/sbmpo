//! [full]
//! [include]
#include <array>
#include <iostream>
#include <cmath>
#include <chrono>

#include "model.h"
#include "optimizer/astar.h"
#include "optimizer/weight.h"
#include "sample.h"
#include "sample/halton.h"
#include "sample/pre_sample.h"
#include "sample/distribution.h"
//! [include]
#include "json.h"

using nlohmann::json;

// Example Model and associated datatypes
namespace example {

//! [position]
struct Position2D {
    double x;
    double y;

    Position2D(double x, double y): x(x), y(y) {}
    Position2D(): x(0.), y(0.) {}
};
//! [position]

void to_json(json &j, const Position2D &p) {
    j = json{
        {"x", p.x},
        {"y", p.y}
    };
}

//! [state]
struct State {
    Position2D pos;
    double T;

    State(Position2D p, double T): pos(p), T(T) {}
    State(double x, double y, double T): pos({x, y}), T(T) {}
    State(double x, double y): pos({x, y}), T(0.0) {}
    State() {}
};
//! [state]

void to_json(json &j, const State &s) {
    j = json{
        { "pos", s.pos }
    };
}

//! [control]
struct Control {
    static const size_t DIM = 2;

    Position2D step;

    Control(Position2D step): step(step) {}
    Control() {}
};
//! [control]

void to_json(json &j, const Control &c) {
    j = json{
        { "step", c.step }
    };
}

Position2D operator+(const Position2D &a, const Position2D &b) {
    return Position2D{ a.x+b.x, a.y+b.y };
}

inline double distance(const Position2D &a, const Position2D &b) {
    return std::sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y));
}

//! [example model]
// Inside namespace example
class Model : public model::Model<State, Control> {
public:
    Model(double T, double threshold): time_step(T), threshold(threshold) {}
    ~Model() {}

    virtual void init(const State &s, const Control &c ) override {
        return;
    }

    virtual State integrate(const State &s, const Control &c) override {
        return State{ s.pos + c.step, s.T + this->time_step };
    }

    virtual double cost(const State &current, const State &next) override {
        return distance(current.pos, next.pos);
    }

    virtual double heuristic(const State &current, const State &goal) override {
        return distance(current.pos, goal.pos);
    }

    virtual bool converge(const State &current, const State &goal) override {
        return distance(current.pos, goal.pos) < this->threshold;
    }

    virtual bool is_valid(const State &, const Control &) override {
        return true;
    }

private:
    double threshold;
    double time_step;
};
//! [example model]

} // end namespace example

namespace sample {
    template<>
    struct Sample<example::Control> {
        auto operator() (
            Generator<example::Control::DIM> *gen
        ) -> example::Control {

            distribution::Uniform select(-2, 2);

            auto g = gen->sample();

            return example::Control({
                select(g[0]),
                select(g[1])
            });
        }
    };
}

namespace optimizer {
    void to_json(json &j, const Trajectory<example::Model> &t) {
        j = json{
            {"cost", t.cost},
            {"trajectory", t.trajectory}
        };
    }
}

int main() {
//! [full main]
//! [initialize model]
auto model = example::Model(0.25, 1.0);
//! [initialize model]

//! [cache samples]
std::array<std::pair<unsigned, unsigned>, example::Control::DIM> pairs = {{
    {19, 36}, {13, 20}
}};
auto halton = sample::Halton<example::Control::DIM>(pairs);
auto pre_halton = sample::PreSample<example::Control::DIM>(&halton, 20);
//! [cache samples]

//! [initialize sample functor]
auto sample = sample::Sample<example::Control>();
//! [initialize sample functor]

std::array<example::Control, 20> controls;
for (size_t i = 0; i < pre_halton.size(); ++i) {
    controls[i] = sample(&pre_halton);
}
json samples = controls;
std::cout << std::setw(4) << samples;

//! [define start point and goal]
auto start = example::State({0., 0.});
auto goal = example::State({10., -10.});
//! [define start point and goal]

//! [initialize astar]
using AStar = optimizer::AStar<example::Model>;

auto astar = AStar(pre_halton.size());
//! [initialize astar]

auto t_start = std::chrono::high_resolution_clock::now();
//! [perform optimization]

auto result = astar.optimize(model, start, goal, &pre_halton);
//! [perform optimization]
auto t_end = std::chrono::high_resolution_clock::now();
auto a_dt = std::chrono::duration<double, std::milli>(t_end-t_start).count();

//! [weighted astar]
// Inflate the heuristic
using WAStar = optimizer::AStar<
    example::Model,
    grid::NoGrid<example::State, optimizer::AStar<example::Model>::Node>,
    optimizer::weight::WeightBy
>;

auto weight = optimizer::weight::WeightBy(2.0);

auto wastar = WAStar(pre_halton.size(), sample, weight);
//! [weighted astar]

t_start = std::chrono::high_resolution_clock::now();
//! [weighted optimize]

auto weighted_result = wastar.optimize(model, start, goal, &pre_halton);
//! [weighted optimize]
t_end = std::chrono::high_resolution_clock::now();
auto w_dt = std::chrono::duration<double, std::milli>(t_end-t_start).count();
//! [full main]

json result_json = result;
std::cout << std::setw(4) << result_json;

//! [astar result]
// result = Trajectory {
//     cost: 13.3333,
//     trajectory: {
//         { State { pos: { x: 0, y: 0 } }, Control { step: { x: 0, y: 0 } } },
//         { State { pos: { x: 0.127424, y: -0.106509 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 0.254848, y: -0.213018 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 0.382271, y: -0.319527 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 0.509695, y: -0.426036 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 0.637119, y: -0.532544 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 0.764543, y: -0.639053 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 0.891967, y: -0.745562 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 1.01939, y: -0.852071 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 1.14681, y: -0.95858 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 1.27424, y: -1.06509 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 1.40166, y: -1.1716 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 1.52909, y: -1.27811 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 3.13019, y: -3.2071 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 3.25762, y: -3.31361 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 3.38504, y: -3.42012 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 3.51247, y: -3.52663 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 3.63989, y: -3.63314 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 5.241, y: -5.56213 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 5.36842, y: -5.66864 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 5.49584, y: -5.77515 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 5.62327, y: -5.88166 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 7.22438, y: -7.81065 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 7.3518, y: -7.91716 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 7.47922, y: -8.02367 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 7.60665, y: -8.13018 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 7.73407, y: -8.23669 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 7.8615, y: -8.3432 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 7.98892, y: -8.4497 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 8.11634, y: -8.55621 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 8.24377, y: -8.66272 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 8.37119, y: -8.76923 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 8.49861, y: -8.87574 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 8.62604, y: -8.98225 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 8.75346, y: -9.08876 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 8.88089, y: -9.19527 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 9.00831, y: -9.30178 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 9.13573, y: -9.40828 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//         { State { pos: { x: 9.26316, y: -9.51479 } }, Control { step: { x: 0.127424, y: -0.106509 } } },
//     }
// }
//! [astar result]

result_json = weighted_result;
std::cout << std::setw(4) << result_json;

//! [wastar result]
// weighted_result = Trajectory {
//     cost: 14.1344,
//     trajectory: {
//         { State { pos: { x: 0, y: 0 } }, Control { step: { x: 0, y: 0 } } },
//         { State { pos: { x: 1.60111, y: -1.92899 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 3.20222, y: -3.85799 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 4.80332, y: -5.78698 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 6.40443, y: -7.71598 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 8.00554, y: -9.64497 } }, Control { step: { x: 1.60111, y: -1.92899 } } },
//         { State { pos: { x: 9.59557, y: -9.46746 } }, Control { step: { x: 1.59003, y: 0.177515 } } },
//     }
// }
//! [wastar result]
} // END MAIN
//! [full]
