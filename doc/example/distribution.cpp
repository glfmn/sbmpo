//! [full]
#include "sample/distribution.h"
#include <cassert>
#include <limits>

/// No indentation in main because indentation appears in documentation
/// without context

//! [enum]
enum Direction {
    N=0, NE, E, SE, S, SW, W, NW
};
//! [enum]

int main() {
{ // Scope to prevent name collisions
//! [uniform distribution]
using sample::distribution::Uniform;

double min = 0.5;
double max = 15;
auto uni = Uniform(min, max);

auto eps = std::numeric_limits<double>::epsilon();
assert(uni(0) - min < eps);
assert(uni(1) - max < eps);
//! [uniform distribution]
}
{
//! [uniform int distribution]
using sample::distribution::UniformInt;

int min = 5;
int max = 10;
auto uni = UniformInt(min, max);

assert(uni(0) == min);
assert(uni(1) == max);
//! [uniform int distribution]
//! [pick from enum]
UniformInt direction(0, 7);

Direction d = (Direction) direction(0.5);
assert(SE == d);
//! [pick from enum]
}
}
//! [full]
