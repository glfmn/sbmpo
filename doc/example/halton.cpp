#include <array>
#include <utility>

//! [include]
#include "sample.h" // generator interface
#include "sample/halton.h" // halton structs
//! [include]

int main() {

//! [default]
auto halton3D = sample::Halton<3>();
//! [default]

//! [from sequence]
std::array<sample::HaltonSequence, 4> sequence = {
    sample::HaltonSequence(17, 1),
    sample::HaltonSequence(19, 1),
    sample::HaltonSequence(37, 1),
    sample::HaltonSequence(41, 1)
};

auto halton4D = sample::Halton<4>(sequence);
//! [from sequence]

//! [from pairs]
std::array<std::pair<unsigned, unsigned>, 5> pairs = {{
    {5, 8}, {79, 40}, {37, 13}, {107, 5}, {7, 20} // {base, index}
}};

auto halton5D = sample::Halton<5>(pairs);
//! [from pairs]

//! [as generator]
auto halton = sample::Halton<5>();

sample::Generator<5> *g = &halton;

auto point = g->sample();
//! [as generator]

}
