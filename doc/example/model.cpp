//! [full]
#include <cmath>

#include "model.h"

//! [Use a namespace]
// Example Model and associated datatypes
namespace example {
//! [Use a namespace]


//! [Defining State & Control]
struct Position2D {
    double x;
    double y;

    Position2D(double x, double y): x(x), y(y) {}
};

struct State {
    Position2D pos;
    double T;

    State(Position2D p, double T): pos(p), T(T) {}
};

struct Control {
    Position2D step;
};
//! [Defining State & Control]


//! [Convinience Methods]
Position2D operator+(const Position2D &a, const Position2D &b) {
    return Position2D{ a.x+b.x, a.y+b.y };
}

inline double distance(const Position2D &a, const Position2D &b) {
    return std::sqrt((a.x*b.x)*(a.x*b.x) + (a.y*b.y)*(a.y*b.y));
}
//! [Convinience Methods]

//! [Subclass Model]
// namespace allows us to differentiate
class Model : public model::Model<State, Control> {
//! [Subclass Model]
public:
    Model(double T, double threshold): time_step(T), threshold(threshold) {}
    ~Model() {}

    virtual void init(const State &s, const Control &c ) override {
        return;
    }

    //! [Convinient Implementation]
    // Notice they are all one line
    virtual State integrate(
        const State &state,
        const Control &control
    ) override {
        return State{ state.pos + control.step, state.T + this->time_step };
    }

    virtual double cost(
        const State &current,
        const State &next
    ) override {
        return distance(current.pos, next.pos);
    }

    virtual double heuristic(
        const State &current,
        const State &goal
    ) override {
        return distance(goal.pos, current.pos);
    }

    virtual bool converge(const State &current, const State &goal) override {
        return distance(current.pos, goal.pos) < this->threshold;
    }
    //! [Convinient Implementation]

    virtual bool is_valid(const State &s, const Control &c) override {
        return true;
    }

private:
    double threshold;
    double time_step;
};

} // end namespace example
//! [full]

int main() {
    example::Model model(0.25, 0.0001);
}
