//! [full]
#include <cassert>
#include <vector>
#include <array>
//! [include]
#include "sample.h"
#include "sample/pre_sample.h"
//! [include]
#include "sample/halton.h"

int main() {

//! [from halton]
auto halton = sample::Halton<3>();

auto pre_halton = sample::PreSample<3>(&halton, 10);

assert(10 == pre_halton.size());
//! [from halton]

//! [simple]
// Empty cache of pre-sampled points
auto pre = sample::PreSample<3>();
//! [simple]

//! [extend]
assert(0 == pre.size());

std::array<double, 3> zero = {0, 0, 0};
pre.push_back(zero);

//! [extend by vector]
assert(1 == pre.size());
//! [extend]

//! [alias]
using Point = sample::PreSample<3>::Point;

std::vector<Point> points = {
    Point{1,1,1},
    Point{2,2,2}
};

pre.push_back(points); // extend pre-sample cache
//! [alias]

//! [extend by generator]
assert(3 == pre.size());
//! [extend by vector]

// Extend with a halton generator
pre.push_back(&halton, 10);

assert(13 == pre.size());
//! [extend by generator]

//! [empty returns 0]
auto empty = sample::PreSample<3>();
auto p = empty.sample();

assert(p[0] == 0 && p[1] == 0 && p[2] == 0);
//! [empty returns 0]

}
//![full]
