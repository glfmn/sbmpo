//! [full]
#include <iostream>
//! [includes]
#include <array>

#include "sample.h"
//! [includes]
#include "sample/halton.h"


//! [define a control]
namespace kinematic {
    // The type which represents the controls of our model
    struct Control {
        double step_x;
        double step_y;

        // Simple constructor
        Control(double x, double y): step_x(x), step_y(y) {}

        // Necessary to know how many dimensions to sample
        static const size_t DIM = 2;
    };
}
//! [define a control]


//! [template specialization]
namespace sample {

    template<>
    struct Sample<kinematic::Control> {
        inline auto operator()(
            Generator<kinematic::Control::DIM> *gen
        ) -> kinematic::Control const {

            // Sample a DIM dimensional point from our generator
            std::array<double, kinematic::Control::DIM> g = gen->sample();

            // You can do whatever you want with g[0] and g[1]
            return kinematic::Control{
                g[0],
                g[1]
            };
        }
    };

}
//! [template specialization]
//![full]


int main() {
//! [halton example]
using kinematic::Control;

sample::Sample<Control> sample;

sample::Generator<Control::DIM> *halton = new sample::Halton<Control::DIM>();

for (unsigned i = 0; i < 10; ++i) {
    Control c = sample(halton);
    std::cout << i << ": (" << c.step_x << ", " << c.step_y << ")\n";
}
//! [halton example]

//! [output]
// 0: (0.5, 0.333333)
// 1: (0.25, 0.666667)
// 2: (0.75, 0.111111)
// 3: (0.125, 0.444444)
// 4: (0.625, 0.777778)
// 5: (0.375, 0.222222)
// 6: (0.875, 0.555556)
// 7: (0.0625, 0.888889)
// 8: (0.5625, 0.037037)
// 9: (0.3125, 0.37037)
//! [output]
}
