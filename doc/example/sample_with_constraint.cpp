//! [includes]
#include <array>

#include "sample.h"
#include "sample/distribution.h"
//! [includes]

#include "sample/halton.h"
#include <iostream>

//! [define a control]
namespace kinematic {
    struct MinMax {
        double min;
        double max;

        MinMax(double min, double max): min(min), max(max) {};
    };

    // The type which represents the constraints we want to use in sampling
    struct ControlConstraint {
        MinMax step_x;
        MinMax step_y;

        ControlConstraint(MinMax x, MinMax y): step_x(x), step_y(y) {}
    };

    // The type which represents the controls of our model
    struct Control {
        double step_x;
        double step_y;

        // Specify which type will be used as the constraint
        using Constraint = ControlConstraint;

        // Simple constructor
        Control(double x, double y): step_x(x), step_y(y) {}

        // Necessary to know how many dimensions to sample
        static const size_t DIM = 2;
    };
}
//! [define a control]

//! [template specialization]
namespace sample {

    template<>
    struct Sample<kinematic::Control> {
        // Define a convinience alias
        using Constraint = kinematic::Control::Constraint;

        Sample(Constraint c): constraint(c) {};

        Constraint constraint;

        inline auto operator()(
            Generator<kinematic::Control::DIM> *gen
        ) -> kinematic::Control const {

            // Define our distributions
            using distribution::Uniform;
            auto step_x = Uniform(
                this->constraint.step_x.min,
                this->constraint.step_x.max
            );
            auto step_y = Uniform(
                this->constraint.step_y.min,
                this->constraint.step_y.max
            );

            // Sample a DIM dimensional point from our generator
            std::array<double, kinematic::Control::DIM> g = gen->sample();

            return kinematic::Control{
                step_x(g[0]),
                step_y(g[1])
            };
        }
    };

}
//! [template specialization]


int main() {
//! [initialize]
using kinematic::Control;

auto c = kinematic::ControlConstraint({-10, 10}, {0, 5});

auto sample = sample::Sample<kinematic::Control>(c);

sample::Generator<Control::DIM> *halton = new sample::Halton<Control::DIM>();

for (unsigned i = 0; i < 10; ++i) {
    Control c = sample(halton);
    std::cout << i << ": (" << c.step_x << ", " << c.step_y << ")\n";
}
//! [initialize]

//! [output]
// 0: (0, 1.66667)
// 1: (-5, 3.33333)
// 2: (5, 0.555556)
// 3: (-7.5, 2.22222)
// 4: (2.5, 3.88889)
// 5: (-2.5, 1.11111)
// 6: (7.5, 2.77778)
// 7: (-8.75, 4.44444)
// 8: (1.25, 0.185185)
// 9: (-3.75, 1.85185)
//! [output]
}
