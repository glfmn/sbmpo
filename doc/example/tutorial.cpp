//! [full]
#include <cassert>
#include <chrono>
#include <random>
//! [full model example]
//! [full sample example]
#include <iostream>
//! [include]
#include <cmath>

#include "model.h"
#include "sample.h"
#include "sample/distribution.h"
#include "sample/pre_sample.h"
#include "optimizer/astar.h"
//! [include]

//! [use a namespace]

namespace discrete_grid {
//! [use a namespace]

//! [define state]
// Discrete x,y position
struct State {
    int x;
    int y;
};

auto distance(const State &a, const State &b) -> double {
    return std::sqrt(double((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y)));
}
//! [define state]

//! [define control]
// Set of valid grid directions
enum Direction {
    N=1, NE, E, SE, S, SW, W, NW
};

// Step in given direction
struct Control {
    Direction step;

    Control(): step(Direction::N) {}
    Control(Direction d): step(d) {}

    static const size_t DIM = 1;
};
//! [define control]

//! [relate control and state]
// Step a State in a direction by adding the Direction
auto operator+(const State &rhs, Direction lhs) -> State {
    auto next = rhs;

    switch (lhs) {
    case Direction::N:
        next.y += 1;
        break;
    case Direction::NE:
        next.y += 1;
        next.x += 1;
        break;
    case Direction::E:
        next.x += 1;
        break;
    case Direction::SE:
        next.y -= 1;
        next.x += 1;
        break;
    case Direction::S:
        next.x -= 1;
        break;
    case Direction::SW:
        next.x -= 1;
        next.y -= 1;
        break;
    case Direction::W:
        next.x -= 1;
        break;
    case Direction::NW:
        next.x -= 1;
        next.y += 1;
        break;
    }

    return next;
}
//! [relate control and state]

//! [print control and state]
std::ostream& operator<<(std::ostream &os, State &s) {
    os << "State { x: " << s.x << ", y: " << s.y << " }";
    return os;
}

std::ostream& operator<<(std::ostream &os, Control &c) {
    os << "Control { step: ";
    switch (c.step) {
    case Direction::N: os << "N"; break;
    case Direction::NE: os << "NE"; break;
    case Direction::E: os << "E"; break;
    case Direction::SE: os << "SE"; break;
    case Direction::S: os << "S"; break;
    case Direction::SW: os << "SW"; break;
    case Direction::W: os << "W"; break;
    case Direction::NW: os << "NW"; break;
    }
    os << "(" << c.step << ") }";
    return os;
}
//! [print control and state]

//! [start model implementation]

//! [full model]
class Model : public model::Model<State, Control> {
public:
//! [start model implementation]
    //! [typedefs]
    using State = discrete_grid::State;

    using Control = discrete_grid::Control;
    //! [typedefs]

    //! [init]
    virtual void init(const State &s, const Control &c ) override {
        return;
    }
    //! [init]

    //! [integrate]
    virtual State integrate(
        const State &state,
        const Control &control
    ) override {
        return state + control.step;
    }
    //! [integrate]

    //! [cost function]
    virtual double cost(
        const State &current,
        const State &next
    ) override {
        return distance(current, next);
    }
    //! [cost function]

    //! [heuristic]
    virtual double heuristic(
        const State &current,
        const State &goal
    ) override {
        return distance(goal, current);
    }
    //! [heuristic]

    //! [converge]
    virtual bool converge(const State &current, const State &goal) override {
        return current.x == goal.x && current.y == goal.y;
    }
    //! [converge]

    //! [is_valid]
    virtual bool is_valid(const State &s, const Control &c) override {
        return true;
    }
    //! [is_valid]
};
//! [full model]

} // end namespace discrete_grid
//! [full model example]

//! [support sampling]
namespace sample {
    template<>
    struct Sample<discrete_grid::Control> {
        auto operator() (
            Generator<discrete_grid::Control::DIM> *gen
        ) -> discrete_grid::Control {

            distribution::UniformInt select(1, 8);

            auto g = gen->sample();

            return discrete_grid::Control(
                (discrete_grid::Direction) select(g[0])
            );
        }
    };
}
//! [support sampling]
//! [full sample example]

int main() {

//! [initialize generator]
auto pre = sample::PreSample<1>();
using Point = sample::PreSample<1>::Point;

for (int i = 0; i < 8; ++i) {
    pre.push_back(Point{ double(i+1)/8 });
}
//! [initialize generator]

//! [step example]
using discrete_grid::State;
using discrete_grid::Direction;

auto origin = State{0, 0};
auto ne = origin + Direction::NE;

assert(ne.x == 1 && ne.y == 1);
//! [step example]

//! [initialize sample functor]
sample::Sample<discrete_grid::Control> sample;
//! [initialize sample functor]


//! [initialize astar]
using AStar = optimizer::AStar<discrete_grid::Model>;
auto astar = AStar(
    pre.size(), // Set branchout size to be number of pre-sampled points
    sample // Function object which converts points to Controls
);
auto model = discrete_grid::Model();
//! [initialize astar]

//! [define start and end]
discrete_grid::Model::State start = {0, 0};

// Create random distribution for end points
std::random_device rd;
std::mt19937 rng(rd());
std::uniform_int_distribution<int> range(-10,10);

discrete_grid::Model::State end = {range(rng), range(rng)};
//! [define start and end]

auto t_start = std::chrono::high_resolution_clock::now();
//! [calculate optimal path]

auto result = astar.optimize(model, start, end, &pre);
//! [calculate optimal path]

auto t_end = std::chrono::high_resolution_clock::now();
double dt = std::chrono::duration<double, std::milli>(t_end-t_start).count();

//! [print result]
std::cout << "Result {\n";
std::cout << "    compute time: "<< dt << " ms,\n";
std::cout << "    cost: "<< result.cost << ",\n";
std::cout << "    trajectory: {\n";
for (auto step : result.trajectory) {
    std::cout << "        { " << step.first << ", " << step.second << " },\n";
}
std::cout << "    }\n}\n";
//! [print result]

//! [example result]
// Results with end state of { x: 10, y: 10} could look like:
// Result {
//     compute time: 0.183437 ms,
//     cost: 77.7817,
//     trajectory: {
//         { State { x: 0, y: 0 }, Control { step: N(1) } },
//         { State { x: 1, y: 1 }, Control { step: NE(2) } },
//         { State { x: 2, y: 2 }, Control { step: NE(2) } },
//         { State { x: 3, y: 3 }, Control { step: NE(2) } },
//         { State { x: 4, y: 4 }, Control { step: NE(2) } },
//         { State { x: 5, y: 5 }, Control { step: NE(2) } },
//         { State { x: 6, y: 6 }, Control { step: NE(2) } },
//         { State { x: 7, y: 7 }, Control { step: NE(2) } },
//         { State { x: 8, y: 8 }, Control { step: NE(2) } },
//         { State { x: 9, y: 9 }, Control { step: NE(2) } },
//         { State { x: 10, y: 10 }, Control { step: NE(2) } },
//     }
// }
//! [example result]

} // END main
//! [full]
