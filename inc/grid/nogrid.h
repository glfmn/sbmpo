#pragma once

#include <utility>

/// \brief Gridding primitives to determine overlaps in paths
namespace grid {

/// \brief Grid which does not store any data or check collisions
///
/// This class provides opt-out behaviour for inserting items into a grid to
/// determine where collisions or overlaps happen.
///
/// In other words, rather than grid, do nothing.
template<typename K, typename V>
class NoGrid {
public:
    /// \brief Type which indexes the grid
    ///
    /// The key is used to determine when a collision has happened against
    /// another key
    using Key = K;

    /// \brief Type which the grid stores
    using Item = V;

    /// \brief type pointed to by an iterator
    using value_type = std::pair<const Key, Item>;

    NoGrid() {};
    ~NoGrid() {};

    /// \brief Insert an element referring to the corresponding key
    /// \param value A key value peer
    /// \return A pair where the first value points to the argument, and the
    ///  second value is always false
    ///
    /// Does no insertion and so always fails, and the element that cause the
    /// insertion to fail is always the value that we tried to insert.
    inline auto insert(
        const value_type &value
    ) -> std::pair<const value_type * const, bool> {
        return std::make_pair(&value, false);
    }

    /// \brief Replace element located at the key with the given value
    ///
    /// Does nothing because all elements are in unique cells which means it is
    /// impossible to ever place two items into the same cell, even with
    /// identicle keys.
    inline void replace(const Key, const Item) {}
};

} // end namespace grid
