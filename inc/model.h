/// \file
/// \brief Model interface

#pragma once

/// @brief Model interface
///
/// \tableofcontents
///
/// The model defines the problem by determining:
///
/// - the the model outputs; the planning space
/// - the inputs which must be sampled
/// - how to evalute and estimate the cost of actions
/// - when sampled actions are valid or invalid
/// - when we have reached our goal and can terminate planning
///
/// \section model-example Full Example Model
///
/// Refer to the `Model` documentation for full detail on the interface.
///
/// \snippet example/model.cpp full
namespace model {

/// \ingroup model
/// @brief Interface between SBMPO and the model to optimize
/// \tparam S State, or planning space of the model
/// \tparam C Control, or input space of the model
///
/// The model defines how costs are estimated and calculated, the mapping
/// between controls and states, and the validity and termination conditions of
/// our problem.
///
/// @section types States & Controls
///
/// `State`: the paramters which make the planning space of the model; the
/// output space of the model.
///
/// `Control`: the paramters we can control to change the state of the model;
/// the input space of the model.
///
/// States and Controls are arbitrily user-defined by template parameter to
/// allow the most readable and flexible code in addition to reuse of
/// pre-defined types and more seamless integration with existing libraries.
///
/// \warning Model::State and Model::Control must have default constructors.
///
/// @subsection example Example
///
/// Lets consider the following model:
///
/// - `State`: a 2D position
/// - `Control`: a vector which represents a step in 2D space
/// - cost function: euclidian distance between states
/// - heuristic: euclidian distance between the current state and goal state
/// - convergence: true when current state is closer to goal than some threshold
///
/// First, make a namespace which describes the name of our model.
///
/// \snippet example/model.cpp Use a namespace
///
/// Then, define a simple data type to help represent the internals of States
/// and Controls, and use it to define states and controls.
///
/// \snippet example/model.cpp Defining State & Control
///
/// Add some functions that will help implement the Model's methods:
///
/// \snippet example/model.cpp Convinience Methods
///
/// Define the model, taking care to pass the previously defined State and
/// Control types as template parameters to the base class.
///
/// \snippet example/model.cpp Subclass Model
///
/// Finally implement the model with the functions and data types previously
/// defined to describe the problem.  This allows for very streamlined and
/// descriptive implementations which prioritize readable and maintainable code.
///
/// \snippet example/model.cpp Convinient Implementation
///
/// \note When chosing a standard library type to use for the Model::States and
/// Model::Controls, only use `std::vector<T>` when you **absolutely** need to
/// change the number of elements at runtime; instead prefer `std::array<T,N>`.
/// `std::array<T,N>` does not own heap allocated resources so it performs far
/// better for our purposes.
///
/// When defining States and Controls, focus on making clearly readable fields
/// and defining a set of utility functions that assist with implementing the
/// Model's methods.
template<typename S, typename C>
class Model {
public:
    virtual ~Model() = default;

    /// @brief Representation of the output or planning space
    typedef S State;

    /// @brief Representation of the input space or sample space
    typedef C Control;

    /// @brief Read and set initial conditions
    ///
    /// Called once planning starts, giving the model access to the first state
    /// and control to perform a sometimes necessary initialization step, such
    /// as:
    ///
    /// - removing obstacles close to the initial state
    /// - calculating extra initial values for the model that depend on the
    ///   values of the first state
    /// - validating the initial state and controls against problem constraints
    virtual void init(
        const Model::State &state,
        const Model::Control &control
    ) = 0;

    /// @brief Generate a state from a control to apply to a previous state
    /// \param state current state
    /// \param control Input appplied to determine the next state
    /// \return New generated state
    ///
    /// Since States are not generated directly and expand from previous States,
    /// a function is necessary which maps from previous states to new states
    /// according to the generated control.
    ///
    /// In other words, this method defines how states propagate or "integrate"
    /// from previous states according to the controls specified by the model.
    ///
    /// For example, if we plan with position as our States, but control the
    /// system in terms of velocity, we must have a function to apply the
    /// velocity to an existing position to get the next position.
    ///
    virtual Model::State integrate(
        const Model::State &state,
        const Model::Control &control
    ) = 0;

    /// @brief Determine the cost between two states
    /// \param current the state to traverse from
    /// \param next the state to traverse to
    ///
    /// Given a current state and future state, to find the optimal path in
    /// terms of a particular state parameter or state parameters, the cost
    /// function provides a method to quantify and _compare_ different paths,
    /// chosing the path that results in the lowest overall cost in terms of
    /// this cost function.
    ///
    /// The canonical cost function for many path-finding applications is simply
    /// the euclidian distance (or some other type of distance calculation),
    /// which results in a distance optimizing path.  However, SBMPO exposes
    /// methods to do optimization over arbitrary state paramters by using the
    /// internal state of the model, such as:
    ///
    /// - energy consumption
    /// - traversal time
    /// - elevation change
    /// - dollars spent
    virtual double cost(
        const Model::State &current,
        const Model::State &next
    )= 0;

    /// @brief Estimate of future costs from the current state
    /// \param current the state to traverse from
    /// \param goal the overall goal node to estimate the future costs from
    ///
    /// Given the current state and the goal state, what can we estimate the
    /// future costs will be?  The heuristic determines where the most fertile
    /// paths to search exist, assuming that continuing along a direct path to
    /// the goal will result in the most efficient overall solution.  This
    /// ensures that paths which take a less direct route are explored last.
    ///
    /// The canonical heuristic function is often also the euclidian distance
    /// from the current state to the goal state.
    ///
    /// The heuristic works best when its units are the same--or at least in the
    /// same order of magnitude--as the cost.
    ///
    /// \warning The heuristic must be admissable or optimistic to get optimal
    /// results; that is, the heuristic **must never over-estimate the cost**
    /// of a future path.  Over-estimation breaks optimality guarantees.
    /// Furthermore the heuristic must never return a negative value.
    virtual double heuristic(
        const Model::State &current,
        const Model::State &goal
    ) = 0;

    /// @brief Termination or convergence condition testing
    ///
    /// Test the current State against the goal to determine if it meets the
    /// convergence criteron against the goal State.
    ///
    /// \param current State to test against for convergence against solution
    /// \param goal State which represents the solution
    /// \return True when a solution is found
    virtual bool converge(
        const Model::State &current,
        const Model::State &goal
    ) = 0;

    /// @brief Check validity of a State against the Model
    /// \param current the state to check for validity
    /// \param control control which leads to current state
    /// \return true if current State passes validity check
    ///
    /// Used to ensure that the current state and control do not meet some
    /// breaking conditions such as colliding with an obstacle.  If a collision
    /// would occur, returning false flags the current state as invalid, for
    /// example.
    virtual bool is_valid(
        const Model::State &current,
        const Model::Control &control
    ) = 0;
};

} // namespace model
