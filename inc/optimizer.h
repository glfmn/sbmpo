/// \file
/// \brief Optimizer interface

#pragma once

#include <vector>
#include <initializer_list>

#include "sample.h"

/// @brief Optimization algorithms
///
/// Optimizers are (usually) graph search algorithms which can be used as the
/// driver behind SBMPO.  They tie all of the other interfaces together and are
/// the objects you instantiate to perform the real work of SBMPO.
///
/// Traditionally, SBMPO uses [A*], but there are many [A*]-based algorithms
/// which also work, and this interface ties them all together.
///
/// [A*]: https://en.wikipedia.org/wiki/A*_search_algorithm
namespace optimizer {

/// \ingroup optimizer
/// @brief Result of SBMPO's operation
///
/// A trajectory which carries the cost of its execution, and all of the steps
/// as pairs of states and controls, who's types are determined by the Model.
///
/// \tparam M The model which defines the state and control types of the
///     trajectory.
template<typename M>
struct Trajectory {
    /// Alias which defines the type of a single step in the trajectory
    ///
    /// The type of a each node in the trajectory
    typedef std::pair<typename M::State, typename M::Control> Step;

    /// @brief total path cost
    double cost;

    /// @brief trajectory as a set of state/control pairs
    std::vector<Step> trajectory;

    /// Default constructor for trajectory
    Trajectory(): cost(0.0), trajectory({}) {}

    /// @brief Construct trajectory from a list of steps and total cost
    ///
    /// ~~~{.cpp}
    /// Trajectory<Model> traj = {start.cost, {{state.start, state.control}}};
    /// // or
    /// auto traj = Trajectory<Model>(start.cost, {{start.state, start.control}});
    /// ~~~
    ///
    /// This is short-hand for:
    ///
    /// ~~~{.cpp}
    /// optimizer::Trajectory<Model>() traj;
    /// std::vector<std::pair<Model::State,Model::Control>> steps;
    /// steps.push({start.state, goal.control});
    /// traj.trajectory = steps;
    /// traj.cost = start.cost;
    /// ~~~
    ///
    /// Or with an initializer list:
    ///
    /// ~~~{.cpp}
    /// optimizer::Trajectory<Model>() traj;
    /// traj.trajectory = {{start.state, start.control}};
    /// traj.cost = start.cost;
    /// ~~~
    ///
    /// \param cost The cost associated with the particular trajectory
    /// \param trajectory A vector or initializer list which contains the steps
    ///     of our trajectory.
    Trajectory(double cost, std::vector<Step> trajectory):
        cost(cost),
        trajectory(trajectory) {};
};

/// \ingroup optimizer
/// @brief Core algorithm which performs optimization for SBMPO
///
/// The Optimizer is the entry point for SBMPO.  In other words, the Optimizer
/// is the object we instantiate and use to peform SMBPO, and it ties all of the
/// other interfaces together.
///
/// \tparam M the type of model::Model we to use with SBMPO
template<class M>
class Optimizer {
public:
    Optimizer() {};
    virtual ~Optimizer() {};

    /// @brief Calcualte an optimal trajectory with SBMPO
    ///
    /// Using the types defiend by the provided model, we find the optimial
    /// trajectory which connects the start and goal states by sampling controls
    /// using the states.
    ///
    /// \param model a constant reference to the model which we will use
    /// \param start a copy of the start state
    /// \param goal a copy of the goal state which we want to find
    /// \param generator the point sampler we want to use
    virtual Trajectory<M> optimize(
        M &model,
        typename M::State start,
        typename M::State goal,
        sample::Generator<M::Control::DIM> *generator
    ) = 0;
};

} // END NAMESPACE optimizer
