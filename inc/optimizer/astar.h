#pragma once

#include <algorithm>
#include <functional>
#include <queue>
#include <unordered_map>
#include <utility>
#include <vector>

#include "optimizer.h"
#include "optimizer/weight.h"
#include "sample.h"
#include "grid/nogrid.h"

// AStar ipmlementation details
namespace {
    /// Id used to hash and track elements
    ///
    /// Additionally stores the f and g costs as it is also used to prioritize
    /// states and nodes in the search as well as uniquely identify them.
    struct Id {
        /// Unique numeric ID
        size_t id;
        /// Estimated total path cost
        double f;
        /// Cost-to-come to the current node
        double g;

        Id() {}

        /// Constructor which simply takes all values
        Id(size_t id, double f, double g): id(id), f(f), g(g) {}

        /// Overload less-than operator
        ///
        /// Uses the less-than operator to compare the f values of each ID
        const bool operator < (const Id &rhs) const {
            return rhs.f < this->f;
        }

        /// Overload greater-than operator
        ///
        /// Uses the greater-than operator to compare the f values of each ID
        const bool operator > (const Id &rhs) const {
            return rhs.f > this->f;
        }

        const bool operator == (const Id &rhs) const {
            return this->f == rhs.f;
        }

        /// Create an ID using a Weight functor
        /// \tparam Weight type of the weight functor
        ///
        /// Weight functor must
        /// - be default constructable
        /// - overload operator () to take two doubles and return a double:
        ///   that is, take in the g and h cost, and return the f cost.
        template<class Weight>
        static auto with_weight(const Weight &w, size_t id, double g, double h) -> Id {
            // use the weighter functor to calculate our estimated total path
            // cost using whatever method the template parameter has specified
            return Id(id, w(g, h), g);
        }
    };
}

namespace optimizer {

    namespace astar {
        /// Node type used to keep track of parents and rebuild trajectory
        template<class M>
        struct Node {
            /// Node ID
            Id id;

            /// State associated with a particular node
            typename M::State state;

            /// Control associated with a particular node
            typename M::Control control;

            /// Compare Nodes by their id
            const bool operator > (const Node &rhs) const {
                return this->id > rhs.id;
            }

            /// Compare Nodes by their id
            const bool operator < (const Node &rhs) const {
                return this->id < rhs.id;
            }

            Node() {}

            /// Construct a Node from its Id State, and Control
            Node(Id id, typename M::State state, typename M::Control control):
                id(id),
                state(state),
                control(control) {

            }

            /// Create a new node which is a child of the current node
            ///
            /// This means we use the current node's state paramters and such
            /// to generate the child
            template<class Weight>
            auto create_child(
                const Weight &w,
                const size_t id,
                const typename M::Control action,
                const typename M::State goal,
                M& model
            ) -> Node<M> {
                typename M::State child_state = model.integrate(this->state, action);

                // Create our child id with the weight the user specified, which
                // enables us to automatically handle weighted A*
                auto child_id = Id::with_weight<Weight>(
                    w,
                    id,
                    this->id.g + model.cost(this->state, child_state),
                    model.heuristic(child_state, goal)
                );

                // Create and return our Node
                return Node(child_id, child_state, action);
            }
        };
    }


/// \ingroup optimizer
/// \brief A* and WA* based optimization for SBMPO
/// \tparam M
///     the type of Model we to use
/// \tparam Grid
///     A strategy to determine when a path overlaps itself.
/// \tparam Compare
///     A policy for comparing nodes which determines when one is better than
///     another, using the f, g, and h costs.
/// \tparam Weight
///     a policy for changing the weight of g and h values used to calculate f,
///     which effectively allows [A*] to accomodate planning using weighted A*
///     ([WA*]).  Refer to \ref weighted-astar for more information.
///
/// \tableofcontents
///
/// An optimizer based upon [A*], which can also perform Weighted A* ([WA*]).
///
/// \snippet example/astar.cpp initialize astar
///
/// Using the typdef above allows for the customization of AStar with template
/// parameters without making the initialization code significantly less
/// readable.
///
/// Once AStar is initialized, getting a trajectory is as simple as calling
/// AStar::optimize.
///
/// \snippet example/astar.cpp perform optimization
///
/// Which would result in something like:
///
/// \snippet example/astar.cpp astar result
///
/// \sa \ref optimizer-tutorial - A tutorial on using A*
/// \sa \ref sample::Generator - \copybrief sample::Generator
/// \sa \ref sample::PreSample - \copybrief sample::PreSample
/// \sa \ref sample::Halton - \copybrief sample::Halton
/// \sa \ref sample::Sample - \copybrief sample::Sample
/// \sa \ref model::Model - \copybrief model::Model
///
/// \section weighted-astar Weighting A*
///
/// We can weight A* by using a [functor]; that is, any object which overloads
/// `operator()`.  We expect `operator()` with the following call signature:
///
/// ~~~{.cpp}
/// double operator()(double g, double h) const;
/// ~~~
///
/// - `g` is the cost-to-come
/// - `h` is the value of the heuristic
/// - and the return parameter is the total estimated path cost.
///
/// \note If the weight function object is not default constructable, it _must_
/// be passed into the constructor.
///
/// \sa weight::NoWeight - \copybrief weight::NoWeight
/// \sa weight::WeightBy - \copybrief weight::WeightBy
/// \sa weight::WeightGreedy - \copybrief weight::WeightGreedy
///
/// \subsection Using Weighted A* Examples
///
/// Use the template parameters to modify the behaviour of AStar to perform
/// weighted A* and pass in the desired weight; here the heuristic is multiplied
/// by 2 according to the behaviour of weight::WeightBy.
///
/// \snippet example/astar.cpp weighted astar
///
/// From here, using Weighted A* is exactly the same as using A* except that the
/// search is comparatively greedier.
///
/// \snippet example/astar.cpp weighted optimize
///
/// Since the heuristic has more influence over where the search goes, the
/// search concludes faster at the sake of some optimaility, assuming the
/// heuristic is optimal.
///
/// \snippet example/astar.cpp wastar result
///
/// \section grid-astar Gridding in A*
///
/// The Grid is a strategy for determining when two states are close enough to
/// overlap; in other words, the grid determines when a path crosses itself.
///
/// By default, AStar will do no gridding and will essentially function as a
/// tree-based search.
///
/// \sa \ref grid::NoGrid - \copybrief grid::NoGrid
/// \sa \ref AStar::Node
///
/// \section astar-model-example Example Model
///
/// The model used in the example code has 2D states and controls where the
/// state is an `x` and `y` position, and the control is an `x`,`y` step to take
/// from the current position.
///
/// \snippet example/astar.cpp example model
///
/// Where the states and controls are defined as:
///
/// \snippet example/astar.cpp position
/// \snippet example/astar.cpp state
/// \snippet example/astar.cpp control
///
/// [A*]: https://en.wikipedia.org/wiki/A*_search_algorithm
/// [WA*]: https://en.wikipedia.org/wiki/A*_search_algorithm#Bounded_relaxation
/// [functor]: https://www.cprogramming.com/tutorial/functors-function-objects-in-c++.html
template<
    class M,
    class Grid = grid::NoGrid<typename M::State, astar::Node<M>>,
    class Weight = weight::NoWeight,
    template<typename> class Compare = std::less
>
class AStar : public Optimizer<M> {
public:
    /// Alias to the concrete sample::Sample used by AStar
    using Sample = sample::Sample<typename M::Control>;

    /// \brief The type which is stored inside a grid
    ///
    /// \warning
    ///     This type is an implementation detail, it is only exposed to serve
    ///     as a template parameter for a gridding strategy which is only
    ///     responsible for containing this type.  It's API is subject to
    ///     breaking change without warning.
    ///
    /// \snippet example/astar.cpp weighted astar
    ///
    /// \sa grid::NoGrid - \copybrief grid::NoGrid
    using Node = astar::Node<M>;

    /// Initialize A*-based optimizer
    ///
    /// \param branchout number of branches to search at each node expansion
    /// \param grid to use to determine when to merge paths
    /// \param sample function object
    AStar(size_t branchout, Grid grid, Sample sample):
        branchout(branchout),
        grid(grid),
        sample(sample),
        weight(Weight()) {}

    /// Initialize A* with default Grid, Weight, and Sample
    /// \param branchout number of branches to search at each node expansion
    AStar(size_t branchout):
        branchout(branchout),
        grid(Grid()),
        sample(Sample()),
        weight(Weight()) {}

    /// Initialize A* with default Grid and Sample
    /// \param branchout number of branches to search at each node expansion
    /// \param weight weight to use to modify serach behaviour towards
    ///     greediness or conservatism
    AStar(size_t branchout, Weight weight):
        branchout(branchout),
        grid(Grid()),
        sample(Sample()),
        weight(weight) {}

    /// Initialize A* with default Grid
    /// \param branchout number of branches to search at each node expansion
    /// \param
    ///     sample function object which converts points to Controls
    ///     \ref sample::Sample
    /// \param weight weight to use to modify serach behaviour towards
    ///     greediness or conservatism
    AStar(size_t branchout, Sample sample, Weight weight):
        branchout(branchout),
        grid(Grid()),
        sample(sample),
        weight(weight) {}

    /// Initialize A* with no defaults
    /// \param branchout number of branches to search at each node expansion
    /// \param grid to use to determine when to merge paths
    /// \param
    ///     sample function object which converts points to Controls
    ///     \ref sample::Sample
    /// \param weight weight to use to modify serach behaviour towards
    ///     greediness or conservatism
    AStar(size_t branchout, Grid grid, Sample sample, Weight weight):
        branchout(branchout),
        grid(grid),
        sample(sample),
        weight(weight) {}

    /// Initialize A* with default Grid and default Weight
    ///
    /// \param branchout number of branches to search at each node expansion
    /// \param sample function object
    ///
    /// \snippet example/astar.cpp initialize astar
    AStar(size_t branchout, Sample sample):
        branchout(branchout),
        grid(Grid()),
        sample(sample),
        weight(Weight()) {}

    /// Default destructor
    ~AStar() {};

    /// \breif Replace sample functor with one of the same type
    auto with_sample(Sample s) -> AStar<M,Grid,Weight,Compare>& {
        this->sample = s;
        return *this;
    }

    /// \brief Replace grid with a grid of the same type
    auto with_grid(Grid g) -> AStar<M,Grid,Weight,Compare>& {
        this->grid = g;
        return *this;
    }

    /// \brief Replace weight with a different weight of the same type
    auto with_weight(Weight w) -> AStar<M,Grid,Weight,Compare>& {
        this->weight = w;
        return *this;
    }

    /// \brief Set a new branchout
    auto with_branchout(size_t b) -> AStar<M,Grid,Weight,Compare>& {
        this->branchout = b;
        return *this;
    }

    /// \brief Calcualte an optimal trajectory with SBMPO with A*
    /// \param model the model which we will use
    /// \param start the start state
    /// \param goal the desired end of the trajectory
    /// \param generator the sampler which we use to sample input controls
    ///
    /// Using the types defiend by the provided model, find the optimial
    /// trajectory which connects the start and goal states, returning the
    /// states which make up that trajectory along with the control used to
    /// reach that state from the previous one.
    ///
    /// \note The first control in the trajectory is the default control and
    /// has no effect on the rest of the trajectory.
    ///
    /// \snippet example/astar.cpp define start point and goal
    ///
    /// Uses [A*] to plan from the start to the goal.
    ///
    /// \snippet example/astar.cpp perform optimization
    ///
    /// [A*]: https://en.wikipedia.org/wiki/A*_search_algorithm
    virtual auto optimize(
        M &model,
        const typename M::State start,
        const typename M::State goal,
        sample::Generator<M::Control::DIM> *generator
    ) -> Trajectory<M> override final {
        // Create aliases to use generic types with less verbose syntax.
        //
        // These types are all dependent on the template parameters of the
        // Optimizer; that is, they depend on the particular Model we are using,
        // and so we must read their types from the Model.  The normal syntax
        // for doing this is extremely verbose, so we alias them to avoid
        // repetition.
        using AStar = AStar<M, Grid, Weight, Compare>;
        using State = typename M::State;
        using Control = typename M::Control;
        using prority_queue = std::priority_queue<
            // We are prioritizing found nodes in the priority queue
            Node,
            // use vector to store elements in the priority queue
            std::vector<Node>,
            // Prioritize objects using the comparison specified
            Compare<Node>
        >;

        // Our start node is also our goal, terminate early
        if (model.converge(start, goal))
            return Trajectory<M>(0, {{start, Control()}});

        // Initialize comparison functor we use to determine if a child node's g
        // score is better than another's; critical for determining parents for
        // maximal A*
        Compare<double> better;

        auto start_node = Node(
            Id::with_weight<Weight>(this->weight, 0, 0., model.heuristic(start,goal)),
            start,
            Control()
        );

        // Give ourselves somewhere to start planning from
        auto queue = prority_queue();
        queue.push(start_node);

        // Insert our first node into the grid
        this->grid.insert({start_node.state, start_node});

        // Need a way to build implicit minimum-cost-spanning tree of our graph
        std::unordered_map<Id, Node> parent;

        // keep track of numeric Id, and keep incrementing to have a simple
        // unique value
        size_t id_counter = 0;

        while (!queue.empty()) {
            Node current = queue.top();
            queue.pop();

            if (model.converge(current.state, goal)) {
                return AStar::result_trajectory(parent, current, model);
            }

            for (int i = 0; i < this->branchout; i++) {
                // Create node with weighted id from the parent node
                // `template create_child` tells C++ this is a template method
                Node child = current.template create_child<Weight>(
                    this->weight,
                    ++id_counter, // keep unique
                    this->sample(generator),
                    goal,
                    model
                );

                if (!model.is_valid(child.state, child.control))
                    continue;

                // Query the grid against the current child state to find the
                // lowest cost child already in the grid
                //
                // Assumes interface of std::unordered_map::insert
                // - best.second is true if the grid cell is empty
                // - best.first is a poiner to the {state, node} at the cell
                auto best = grid.insert({child.state, child});
                bool is_best = best.second;
                Node best_node = best.first->second;

                if (!is_best && better(best_node.id.g, child.id.g)) {
                    // Skip if we can't beat the best, assuming we found it
                    continue;
                } else {
                    // We are the best or equal
                    parent[child.id] = current;
                    grid.replace(child.state, child);
                    queue.push(child);
                }
            }
        }

        return Trajectory<M>();
    }

private:
    size_t branchout;
    Sample sample;
    Grid grid;
    Weight weight;

    template<class Map>
    static auto result_trajectory(
        const Map &parent,
        Node current,
        M &model
    ) -> Trajectory<M> {
        auto result = Trajectory<M>();

        result.trajectory.push_back({current.state, current.control});
        double cost = 0;
        // build up the trajectory by following the parent nodes
        while (true) {
            // Use `at` to detect a parent doesn't exist using execptions
            try {
                auto p = parent.at(current.id);
                cost += model.cost(current.state, p.state);
                current = p;
                result.trajectory.push_back({current.state, current.control});
            } catch (std::out_of_range) {
                break;
            }
        }

        result.cost = cost;
        std::reverse(result.trajectory.begin(), result.trajectory.end());

        return result;
    }
};

} // END NAMESPACE optimizer


namespace std {
    /// Specialize hash template to allow us to use Id in a hashtable
    ///
    /// Simply use the unique numeric Id found in the Id struct
    template<>
    class hash<Id> {
    public:
        typedef Id argument_type;

        std::size_t operator()(Id const& id) const {
            std::hash<size_t> hash;
            return hash(id.id);
        }
    };


    /// Hash nodes by their id
    ///
    /// Simply use the unique numeric id found in the Node Id.
    template<typename M>
    class hash<optimizer::astar::Node<M>> {
    public:
        typedef optimizer::astar::Node<M> argument_type;

        std::size_t operator()(argument_type const& node) const {
            std::hash<Id> hash;
            return hash(node.id);
        }
    };
}
