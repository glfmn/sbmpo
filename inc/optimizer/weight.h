#pragma once


namespace optimizer {

/// \defgroup weight Weights
/// @{

/// @brief Weights used in A* variants to perform WA*
namespace weight {


/// @brief Do not weight the heuristic
///
/// Simply add the heuristic and the cost-to-come to estimate the total path
/// cost, the default behaviour of AStar.
///
/// A function object which just adds it's arguments.
class NoWeight {
public:
    /// Calculate a standard f value
    /// \param g cost-to-come
    /// \param h estimated future cost
    /// \return **unweighted** sum of `g` and `h`
    inline double operator()(double g, double h) const {
        return g + h;
    }
};

/// Weight the heuristic by a desired value
/// \tparam w
///    The weight to use to weight the heuristic
///
/// Instead of adding the heuristic (h) and the cost-to-come (g) we can
/// weight h to alter the performance characteristics of A*-based planners.
///
/// A function object that multiplies its weight by the second argument and
/// returns the sum.
///
/// Because the total path cost is the value used in the priroity queue, we can
/// generally increase A*'s greadines by increasing the value of the heuristic.
/// However, this sacrifices optimality. Furthermore, we can make A* more
/// conservative as we decrease the value of the heuristic. With a weight of 0,
/// Weighted A* effectively becomes Dijkstra's algorithm because we do not use
/// the heuristic at all.
///
/// \note Using `w < 1` makes the search conservative
///
/// \snippet example/astar.cpp weighted astar
class WeightBy {
public:
    /// Weight the heuristic by the absolute value of `w`
    /// \param g cost-to-come
    /// \param h estimated future cost
    /// \return the weighted sum of `g` and `w*h`
    inline double operator()(double g, double h) const {
        return g + std::abs(w)*h;
    }


    /// \brief Use provided weight against the heruistic
    /// \param w the weight to use when calculating weighted sum
    WeightBy(double w): w(w) {}

    /// \brief Weight by 1.0, effectively doing no weighting at all
    WeightBy(): w(1.0) {}

    /// Weight to against the heuristic
    double w;
};


/// Weight the cost-to-come by the desired value
///
/// Weight the cost-to-come instead of the heuristic to create greedy serach
/// by relying on the heuristic more.  Comes at the cost of sub-optimality.
///
/// \note Using `w < 1` makes the search greedy
class WeightGreedy {
public:
    /// Weight the cost-to-come by the absolute value of `w`
    /// \param g cost-to-come
    /// \param h estimated future cost
    /// \return the weighted sum of `w*g` and `h`
    inline double operator()(double g, double h) const {
        return std::abs(w)*g + h;
    }

    /// \brief Use provided weight against the cost-to-come
    /// \param w the weight to use when calculating weighted sum
    WeightGreedy(double w): w(w) {}

    /// \brief Weight by 1.0, effectively doing no weighting at all
    WeightGreedy(): w(1.0) {}

    /// Weight to use against the cost-to-come
    double w;
};


/// @}


} // END NAMESPACE weight


} // END NAMESPACE optimizer
