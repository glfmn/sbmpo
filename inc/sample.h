/// \file
/// \brief Sample and Generator interfaces

#pragma once

#include <array>

/// \group sample Sampling Interfaces

/// \ingroup sample
/// \brief Sampling interfaces and types
///
/// Sampling is key to the functionality of SBMPO, as it defines how we search
/// our model's input space.  Sampling necessarily introduces some measure of
/// suboptimality into the problem in exchange for making it tractable and
/// practicle to solve.  The choice of what smpling to use and how best to use
/// it remains a key engineering challenge.
///
/// Sampling aims to achieve a few priorities:
///
/// - choice of arbitrary point sampler (`Generator`)
/// - ability to flexibly convert sampled points to a user defined struct
/// - sample data from various distributions
/// - change sampling properties at runtime using constraints
///
/// \section generate-points Sampling Point Data
///
/// A `Generator` samples `D` dimensional points where each dimension has
/// values between 0 and 1.  All of the generators in SBMPO aim to sample this
/// space as uniformly as possible (with low discrepancy) to acheive the best
/// sampling performance.
///
/// \subsection generator-list Generators
///
/// `Generator`s which directly sample points of data:
///
/// - \ref sample::Halton - \copybrief sample::Halton
///
/// \subsection generator-adaptor-list Adaptors
///
/// `Generator`s which alter the functionaly of other `Generator`s:
///
/// - \ref sample::PreSample - \copybrief sample::PreSample
///
/// \subsection generator-example Example usage
///
/// \snippet example/sample_data.cpp halton example
///
/// \section sample-types Sample Custom Types
///
/// The Sample struct's `operator()` implementation converts a sampled point in
/// `D` dimensions to an arbitrary struct via template specialization.
///
/// \subsection full-specilization-example Example Specialization
///
/// \snippet example/sample_data.cpp full
namespace sample {


/// \brief Interface for control sample generation
/// \tparam D sample D dimensional space
///
/// Generators are space filling algorithms which attempt to generate points in
/// `D` dimensions as uniformly as possible.
///
template<size_t D>
class Generator {
public:
    Generator() {};
    virtual ~Generator() = default;

    /// Sample a point in D dimensional space
    virtual std::array<double, D> sample() = 0;
};


/// \brief sample an arbitrary supported type from an arbitrary generator
/// \tparam S the resulting sampled type
///
/// \section customType Sampling an arbitrary struct
///
/// Given some pre-defined type that we want to sample:
///
/// \snippet example/sample_data.cpp define a control
///
/// Providing a template speclization with that type allows the use of _any_
/// Generator to create that type.
///
/// \snippet example/sample_data.cpp template specialization
///
/// For example, Halton sampling:
///
/// \snippet example/sample_data.cpp halton example
///
/// The output may look like:
///
/// \snippet example/sample_data.cpp output
///
/// \subsection runtimeChanges Using Constraints
///
/// While this example is simple, it is limited to the range of points the
/// generator can produce.  Normally, a Generator will sample between 0 and 1 in
/// each dimension, as uniformly as possible, but this may create unusable
/// sampled types which violate some conditions.
///
/// Constraints allow the runtime behaviour to change depending on passed
/// information to improve sampling performance.
///
/// Of course it is possible to hard-code a different sampling behaviour by
/// manipulating the values provided by the generator, but constraints provide
/// additional runtime flexibility which is often useful in practice.
///
/// First we can add the distribution header to help us define this more
/// flexible behaviour.
///
/// \snippet example/sample_with_constraint.cpp includes
///
/// To sample more complicated distributions we use the Constraint type.  A
/// Constraint is a struct which provides information necessary to change
/// sampling behaviour at runtime.  For example, the constraint can contain the
/// minimum and maximum for the values we want to sample.
///
/// \snippet example/sample_with_constraint.cpp define a control
///
/// Then we can use that information in the template specilization to initialize
/// our distributions.  Structs from the distribution header are simple
/// function objects which take a double between 0 and 1 and pick values from
/// various types of distributions.  To keep this example simple, we will use
/// a uniform distribution where each value 0 to 1 becomes a value between
/// `min` and `max`.
///
/// \snippet example/sample_with_constraint.cpp template specialization
///
/// Finally, initialize the Sample function object with the control in the
/// constructor and generate a few samples.
///
/// \snippet example/sample_with_constraint.cpp initialize
///
/// The output may look like:
///
/// \snippet example/sample_with_constraint.cpp output
///
/// \note The `Constraint` typdef and the static `DIM` constant are not 100%
/// necessary on the `Control` struct for the overload, but they are good
/// style and practice as they reduce the number of places we need to update
/// that information if we change the `Control` struct and makes it harder to
/// miss.
template<typename S>
struct Sample {
public:
    /// The type which represents a constraint as defined by the sampled type
    using Constraint = typename S::Constraint;

    Sample();

    /// Accept and store a constraint
    Sample(Constraint c): constraint(c) {};

    /// \brief Sample a type using a specified generator
    /// \param g Generator which produces points in DIM dimensions
    ///
    /// Use the sample generator to construct our sampled type using numerical
    /// data from the generator with the desired number of dimensions.
    ///
    /// \snippet example/sample_data.cpp halton example
    inline S operator()(Generator<S::DIM> *g);

    Constraint constraint;
};

} // END NAMESPACE sampler
