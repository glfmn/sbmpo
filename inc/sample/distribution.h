#pragma once

namespace sample {

/// \defgroup distribution

/// \ingroup sampler
/// \ingroup distribution
/// \brief Utilities for generating distributions from values between 0 and 1
///
/// Map doubles in a uniform distribution between 0 and 1 to different domains
/// or functions.
///
/// \snippet example/distribution.cpp full
namespace distribution {

/// \ingroup distribution
/// @brief Pick values from a uniform range between two numbers
///
/// Convert a value between 0 and 1 to a value between an arbitrary min and max
///
/// \snippet example/distribution.cpp uniform distribution
///
/// \sa UniformInt
struct Uniform {
    /// Minimum value of the uniform distribution
    double min;
    /// Maximum value of the uniform distribution
    double max;

    /// Create uniform distribution from min and max
    Uniform(double min, double max): min(min), max(max) {};

    /// Calculate corresponding double from the range
    /// \return value in the uniform range between min and max
    inline auto operator()(double n) -> double {
        return n * (this->max - this->min) + this->min;
    }
};

/// \ingroup distribution
/// \brief Pick an integer between a min and max
///
/// Convert value between 0 and 1 to an integer between an arbitrary min and max
///
/// \snippet example/distribution.cpp uniform int distribution
///
/// Given some enum:
///
/// \snippet example/distribution.cpp enum
///
/// UniformInt can pick variants.
///
/// \snippet example/distribution.cpp pick from enum
///
/// \sa Uniform
struct UniformInt {
    /// Minimum value of the uniform distribution
    int min;

    /// Maximum value of the uniform distribution
    int max;

    /// Create uniform distribution from min and max
    UniformInt(int min, int max): min(min), max(max) {};

    /// Calculate corresponding integer from the range
    /// \return integer in the uniform range between min and max
    inline auto operator() (double n) -> int {
        auto range = max-min;
        return min + (n * double(range));
    }
};

} // END NAMESPACE distribution

} // END NAMESPACE sample
