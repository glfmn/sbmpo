/// \file
/// \brief Halton sequence Generator

#pragma once

#include <array>
#include <cmath>
#include <vector>

#include "sample.h"

namespace sample {

/// \brief sample numbers incrementally from a single halton base sequence
///
/// \sa sample::Halton
struct HaltonSequence {
private:
    /// Cached remainders used to quickly calculate halton numbers
    std::vector<double> rem;

    /// Cached digits in base b used to quickly calcualte halton numbers
    std::vector<unsigned> dig;

    /// Base of the Halton sequence.
    unsigned base;

public:
    /// Construct halton sequence using base b at index i
    /// \param b base used to calculate the sequence, greater than, or equal to,
    ///     1, usually a prime number
    /// \param i index in the sequence, greater than or equal to 1
    ///
    /// \snippet example/halton.cpp from sequence
    ///
    /// \note Memory use is inversely proportional to the magnitude of the base.
    ///
    /// \sa Halton::Halton(std::array<HaltonSequence,D>)
    /// \sa Halton::Halton(std::array<std::pair<unsigned,unsigned>, D>)
    HaltonSequence(unsigned b, unsigned i);

    /// Construct halton sequence at index 1 with base 2
    HaltonSequence();

    /// Advance the halton sequence to the next value in the sequence
    double advance();
};

/// \ingroup sampler
/// \brief sample points using halton sequence
/// \tparam D number of dimensions to sample in
///
/// \snippet example/halton.cpp include
///
/// The halton sequence, when based upon a prime number, fills space very
/// uniformly and evenly (it has low discrepancy).  Using a different coprime
///  base for each dimension, this effect scales well with dimensions.
///
/// \warning The Halton sampling method does not support more than 100
/// dimensions unless you calculate the primes yourself, but sampling
/// performance degrades more than exponentially with the number of dimensions
/// making Halton sampling generally unusable after 10 dimensions.
///
/// The halton sequence is also deterministic, so it provides predicatble
/// results which is very useful for debugging.  Select a random start index to
/// introduce randomness.
///
/// You can assign a halton point generator to a pointer to a Generator with the
/// same number of dimensions:
///
/// \snippet example/halton.cpp as generator
///
/// \note Memory used is inversely proportional to the magnitude of the base.
///
/// This allows using the Sample function object with Halton sampling to
/// generate arbitrary structs of numerical data with fixed size.
template<size_t D>
class Halton: public Generator<D> {
private:
    /// First 100 primes starting at 2
    ///
    /// Primes used to initialize the halton sequence for each dimension at a
    /// different prime base to ensure best space filling performance in high
    /// dimensions.
    static const std::array<unsigned, 100> PRIMES;

    std::array<HaltonSequence, D> sequence;
public:
    /// Sample the next D dimensional point
    virtual std::array<double, D> sample() override final {
        // NOTE: inlined for now since it's a template
        // The C++ compilation model may require us to place all template
        // definitions in separate .inl files which are included back into the
        // header.  Pending a decision, it's left here.  I prefer inline
        // definitions personally.

        std::array<double, D> point;
        for (int i = 0; i < D; ++i) {
            point[i] = this->sequence[i].advance();
        }

        return point;
    }

    /// Default constructor starting every halton sequence at 1
    ///
    /// \snippet example/halton.cpp default
    Halton() {
        *this = Halton(1);
    }

    /// \brief Initialize Halton point generator
    /// \param index the index of the halton sequence to start in all dimensions
    ///
    /// Automatically selects sequential prime nunbers for each dimension.
    Halton(unsigned index) {
        // Because we are using the static set of 100 primes, ensure that we do
        // not attempt to use more primes than we have.
        static_assert(D <= 100,
            "Dimension must not be greater than 100, use another constructor"
        );

        for (int i = 0; i < D; i++) {
            this->sequence[i] = HaltonSequence(this->PRIMES[i], index);
        }
    }

    /// \brief Create a point generator from a set of Halton Sequences
    /// \param s an array of initialized HaltonSequences
    ///
    /// \snippet example/halton.cpp from sequence
    ///
    /// \sa sample::HaltonSequence
    Halton(std::array<HaltonSequence, D> s): sequence(s) {}

    /// \brief Create a point generator from the specified indecies and bases
    /// \param spec an array of pairs (base, index)
    ///
    /// \snippet example/halton.cpp from pairs
    ///
    /// \sa sample::HaltonSequence
    Halton(std::array<std::pair<unsigned, unsigned>, D> spec) {
        size_t i = 0;
        for (auto s : spec) {
            this->sequence[i] = HaltonSequence(s.first, s.second);
            ++i;
        }
    }
};

template<size_t D>
const std::array<unsigned, 100> Halton<D>::PRIMES = {
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
    73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151,
    157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233,
    239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317,
    331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419,
    421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503,
    509, 521, 523, 541
};

} // end namespace sample
