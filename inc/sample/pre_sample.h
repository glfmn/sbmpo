/** \file
 */

#include "sample.h"

#include <array>
#include <vector>

namespace sample {

/// \brief Cache sampled points and cycle them to save computation
/// \tparam D Dimension of the Generator
///
/// An adaptor which caches points created either manually or from other
/// generators, allowing for entirely custom sample creation, or up-front
/// calculation of all points used, or mixing points from multiple generators.
///
/// A `PreSample`'s cache can be extended in a variety of ways which enables
/// tailoring the set of samples used to meet requirements:
///
/// - \ref PreSample::push_back(Point):
///   \copybrief PreSample::push_back(Point)
/// - \ref PreSample::push_back(std::vector<Point>):
///   \copybrief PreSample::push_back(std::vector<Point>)
/// - \ref PreSample::push_back(Generator<D>*, size_t):
///   \copybrief PreSample::push_back(Generator<D>*, size_t)
///
/// \section pre-sample-usage Example
///
/// \snippet example/pre_sample.cpp full
template<size_t D>
class PreSample: public Generator<D> {
public:
    /// \brief Convinience alias to result of \ref sample::PreSample::sample()
    ///
    /// Also an alias to the type stored in the Cache.
    ///
    /// \snippet example/pre_sample.cpp alias
    using Point = std::array<double, D>;

    /// \brief Create an empty set of pre-sampled points
    ///
    PreSample(): cache(std::vector<Point>()), index(0) {}

    /// \brief Create a set of pre-sampled points from an existing vector
    /// \param cache the points to use for sampling
    ///
    /// \see \ref PreSample::push_back(std::vector<Point>)
    PreSample(std::vector<Point> cache): cache(cache), index(0) {}

    /// \brief PreSample samples from a generator of matching dimension by pointer
    /// \param gen the generator from which to pre-sample points
    ///
    /// Example caching a cache of points using a \ref Halton generator
    ///
    /// \snippet example/pre_sample.cpp from halton
    PreSample(Generator<D> *gen, size_t count):
        cache(std::vector<Point>()), index(0)
    {
        extend(this->cache, gen, count);
    }

    /// \brief Return a pre-sampled point
    /// \return a cached point
    ///
    /// If the PreSample is empty, return a point of all 0s:
    ///
    /// \snippet example/pre_sample.cpp empty returns 0
    virtual auto sample() -> std::array<double, D> override final {
        if (cache.size() < 1) {
            std::array<double, D> point;
            for (size_t i = 0; i < D; ++i) {
                point[i] = 0;
            }
            return point;
        }
        this->index = index % cache.size();
        return cache[this->index++];
    }

    /// \brief Add a point to the set of pre-sampled points
    ///
    /// \snippet example/pre_sample.cpp extend
    void push_back(Point sample) { this->cache.push_back(sample); }

    /// \brief Add multiple points to the set of pre-sampled points
    /// \param samples the samples to add
    ///
    /// \snippet example/pre_sample.cpp extend by vector
    void push_back(std::vector<Point> samples) {
        extend(this->cache, samples);
    }

    /// \brief Add the desired number of samples from a Generator
    /// \param gen the Generator from which to sample points
    /// \param count the number of points to sample
    ///
    /// \snippet example/pre_sample.cpp extend by generator
    void push_back(Generator<D> *gen, size_t count) {
        extend(this->cache, gen, count);
    }

    /// \brief Current count of pre-sampled points
    /// \return number of cached point
    ///
    /// \snippet example/pre_sample.cpp extend by vector
    inline auto size() -> size_t { return this->cache.size(); }

private:
    /// PreSample of sampled points
    std::vector<Point> cache;

    /// Current active sample
    size_t index;

    static void extend(std::vector<Point> &cache, std::vector<Point> &ps) {
        cache.reserve(cache.size() + ps.size());

        for (auto p : ps) {
            cache.push_back(p);
        }
    }

    static void extend(std::vector<Point> &cache, Generator<D> *gen, size_t n) {
        cache.reserve(cache.size() + n);

        for (size_t i = 0; i < n; ++i) {
            cache.push_back(gen->sample());
        }
    }
};

} // end namespace sample
