#include "optimizer/astar.h"


optimizer::astar::Id::Id(size_t id, double f, double g):
    id(id), f(f), g(g) {

}


template<class Weight>
optimizer::astar::Id optimizer::astar::Id::with_weight(
    size_t id,
    double g,
    double h
) {
    // Create our functor
    Weight weighter;

    // use the weighter functor to calculate our estimated total path cost
    // using whatever method the template parameter has specified
    return Id(id, weighter(g, h), g);
}


const bool optimizer::astar::Id::operator < (
    const optimizer::astar::Id &rhs
) const {
    return this->f < rhs.f;
}


const bool optimizer::astar::Id::operator > (
        const optimizer::astar::Id &rhs
    ) const {
    return this->f > rhs.f;
}


namespace std {
    /// Specialize hash template to allow us to use Id in a hashtable
    ///
    /// Simply use the unique numeric Id found in the Id struct
    template<>
    class hash<optimizer::astar::Id> {
    public:
        typedef optimizer::astar::Id argument_type;

        std::size_t operator()(optimizer::astar::Id const& id) const {
            std::hash<size_t> hash;
            return hash(id.id);
        }
    };


    /// Hash nodes by their id
    ///
    /// Simply use the unique numeric id found in the Node Id.
    template<typename M>
    class hash<optimizer::astar::Node<M>> {
    public:
        // typedef to allow templated type as argument type without template on
        // the () operator
        typedef optimizer::astar::Node<M> argument_type;

        std::size_t operator()(argument_type const& node) const {
            std::hash<optimizer::astar::Id> hash;
            return hash(node.id);
        }
    };
}


template<class M>
optimizer::astar::Node<M>::Node(
    optimizer::astar::Id id,
    typename M::State state,
    typename M::Control control
):  id(id),
    state(state),
    control(control) {

}


template<class M>
const bool optimizer::astar::Node<M>::operator < (
    const optimizer::astar::Node<M> &rhs
) const {
    return this->id < rhs.id;
}


template<class M>
const bool optimizer::astar::Node<M>::operator > (
    const optimizer::astar::Node<M> &rhs
) const {
    return this->id > rhs.id;
}


template<class M> template<class Weight>
inline optimizer::astar::Node<M> optimizer::astar::Node<M>::create_child(
    const size_t id,
    const typename M::Control action,
    const typename M::State goal,
    const M& model
) const {
    typename M::State child_state = model.integrate(this->state, action);

    // Create our child id with the weight the user specified, which
    // enables us to automatically handle weighted A*
    auto child_id = Id::with_weight<Weight>(
        id, // keep id unique
        this->id.g + model.cost(this->state, child_state),
        model.heuristic(this->state, goal)
    );

    // Create and return our Node
    return Node(id, child_state, action);
}


template<
    class M,
    class G,
    class Grid,
    class Weight,
    template<typename> class Cmp
>
optimizer::AStar<M,G,Grid,Weight,Cmp>::AStar(
    size_t branchout,
    Grid grid,
    optimizer::AStar<M,G,Grid,Weight,Cmp>::Sample sample
):
    branchout(branchout),
    grid(grid),
    sample(sample) {

}


template<
    class M,
    class G,
    class Grid,
    class Weight,
    template<typename> class Cmp
>
optimizer::AStar<M,G,Grid,Weight,Cmp>::~AStar() {}


template<
    class M,
    class G,
    class Grid,
    class Weight,
    template<typename> class Cmp
>
template<class Map>
optimizer::Trajectory<M>
optimizer::AStar<M,G,Grid,Weight,Cmp>::result_trajectory(
    const Map& parent,
    const typename M::State current
) {
    using Trajectory = typename optimizer::Trajectory<M>();

    auto result = Trajectory();

    double g = current.id.g;
    // build up the trajectory by following the parent nodes
    while (true) {
        // Use `at` to detect a parent doesn't exist when exception is thrown
        try {
            current = parent.at(current.id);
            result.trajectory.push({current.state, current.control});
            g += current.id.g;
        } catch (std::out_of_range) {
            break;
        }
    }

    result.cost = g;

    return result;
}


template<
    class M,
    class G,
    class Grid,
    class Weight,
    template<typename> class Compare
>
optimizer::Trajectory<M> optimizer::AStar<M,G,Grid,Weight,Compare>::optimize(
    const M &model,
    const typename M::State start,
    const typename M::State goal,
    G* generator
) {
    // Create aliases to use generic types with less verbose syntax.
    //
    // These types are all dependent on the template parameters of the
    // Optimizer; that is, they depend on the particular Model we are using,
    // and so we must read their types from the Model.  The normal syntax for
    // doing this is extremely verbose, so we alias them to avoid repetition.
    using AStar = AStar<M,G,Grid,Weight,Compare>;
    using State = typename M::State;
    using Control = typename M::Control;
    using Trajectory = typename optimizer::Trajectory<M>();
    using Id = optimizer::astar::Id;
    using Node = typename optimizer::astar::Node<M>;
    using prority_queue = std::priority_queue<
        // We are prioritizing found nodes in the priority queue
        Node,
        // use vector to store elements in the priority queue
        std::vector<Node>,
        // Prioritize objects using the comparison specified by template param
        Compare<Node>
    >;

    // Our start node is also our goal, terminate early
    if (model.converge(start,goal))
        return Trajectory(0, {{start.state, Control()}});

    // Initialize comparison functor we use to determine if a child node's g
    // score is better than another's; critical for determining parents for
    // maximal A*
    Compare<double> better;

    // keep track of numeric Id, and keep incrementing to have a simple unique
    // value
    size_t id_counter = 0;

    auto start_node = Node(
        Id::with_weight<Weight>(id_counter, 0., model.heuristic(start,goal)),
        start,
        Control()
    );

    // Give ourselves somewhere to start planning from
    prority_queue queue = { start_node };

    // Insert our first node into the grid
    this->grid.insert(start_node.state, start_node);

    // Need a way to build implicit minimum-cost-spanning tree of our graph
    std::unordered_map<Id, Node> parent;

    while (!queue.empty()) {
        Node current = queue.pop();

        if (model.converge(current.state)) {
            return AStar::result_trajectory(parent, current);
        }

        for (int i = 0; i < this->branchout; i++) {
            // Create node with weighted id from the parent node
            // `template create_child` tells C++ this is a template method
            Node child = current.template create_child<Weight>(
                ++id_counter,
                this->sample(generator),
                goal,
                model
            );

            if (!model.is_valid(child.state))
                continue;

            // Query the grid against the current child state to find the
            // lowest cost child already in the grid
            typename Grid::iterator best = grid.find(child.state);

            if (best != grid.end() && better(best->second.id.g, child.id.g)) {
                // Skip if we can't beat the best, assuming we found it
                continue;
            } else {
                // We are the best or equal
                parent[child.id] = current;
                grid[child.state] = child;
            }
        }
    }

    return Trajectory();
}
