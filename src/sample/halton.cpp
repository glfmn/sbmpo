#include "sample/halton.h"

sample::HaltonSequence::HaltonSequence() {
    *this = HaltonSequence(2,1);
}

sample::HaltonSequence::HaltonSequence(unsigned b, unsigned i) {
    // Pre-set size of the halton sequence to ensure we can get to at least the
    // hundredth index past the start before the vectors have to resize
    const size_t size = ceil(log(i+100) / log(double(b)));
    this->rem = std::vector<double>();
    this->rem.reserve(size);
    this->dig = std::vector<unsigned>();
    this->dig.reserve(size);

    // Ensure we have mutable access for a safe starting index
    i = i < 1 ? 0 : i-1;

    // Ensure a sensible base
    b = b < 2 ? 2 : b;

    // Convert number to digits in the given base
    while (i >= b) {
        this->dig.push_back(i % b);
        i = i / b;
    }
    dig.push_back(i);

    // Calculate remainders in reverse order for each digit
    double base = b;
    rem.push_back(0.);
    for (auto d = dig.rbegin(); d != dig.rend(); ++d) {
        if (rem.size() < dig.size()) {
            double last = rem[rem.size()-1];
            rem.push_back((double(*d) + last) / base);
        }
    }

    this->base = base;
}


auto sample::HaltonSequence::advance() -> double {
    // Efficient calculation of the next number with minimal error is performed
    // using Kolar and O'Shea's method for calculating elements of the Halton
    // Sequence.
    // To accomodate the fact that vectors expand from the back, the order of
    // remainders is reversed compared to their method, and bounds checking is
    // performed.
    if (dig[0] == base-1) {
        size_t i = 0;
        // Perform carry operation
        while (i < dig.size() && dig[i] == base-1) {
            dig[i] = 0;
            i += 1;
        }
        // Bounds check when updating element
        if (i < dig.size()) {
            dig[i] += 1;
        } else {
            dig.push_back(1);
            rem.push_back(0.); // keep number of digits and remainders the same
        }

        // Update remainders
        size_t len = rem.size();
        double b = base;
        rem[len-i] = (double(dig[i]) + rem[len-i-1]) / b;
        if (i >= 2) {
            for (i = len-i; i < len-1; ++i) {
                rem[i+1] = rem[i] / b;
            }
        }

        // Calculate new state
        return rem[len-1] / b;
    } else {
        // Calculate new state
        dig[0] += 1;
        return (double(dig[0]) + rem[rem.size()-1]) / double(base);
    }
}
