# Register a google test unit test; the name should match the extensionless name
# of the test file in the test directory.  For example:
#
# file: test/sampler.cpp
#            ^^^^^^^~~~~~ the name
#
# gtest(sampler sbmpo)
#               ^^^^^~~~~ we need to link against SBMPO
function(gtest name libs)
    add_executable("utest_${name}" "${name}.cpp")
    target_link_libraries("utest_${name}" gtest gtest_main gmock ${libs})
    add_test(NAME "${name}_test" COMMAND "utest_${name}")
endfunction()

# Register google tests

# Sampling tests
gtest(sampler sbmpo)
gtest(halton sbmpo)
gtest(pre_sample sbmpo)
gtest(distribution sbmpo)
gtest(no_grid sbmpo)
