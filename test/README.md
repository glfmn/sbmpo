# Quick Start

Assuming you placed your cmake build system in the `target` directory during the
configure step, to run the test suite, from the command-line simply invoke:

```sh
$ cmake --build target && cmake --build target --target test
```

When running tests during the staging process, it is prudent to stash any
unstaged changes before you make your commit as they may affect the correctness
of your code.  Generally, this would be:

```sh
$ git stash save -ku
$ cmake --build target && cmake --build target --target test
$ git commit
$ git stash pop
```

However, _the [pre-commit hook]_ found in the root directory **automates** this
process for you by stashing any unstaged changes, running the test suite, and
then unstashing your changes.  You can skip this on `git commit` with the
`--no verify` flag when writing a complicated patch which required editing hunks
manually, as unstashing changes would resolve in a merge conflict.

# Verbose Test Output

By default, the test target will hide all of the information that's output by
the testing utility to give extra information about why tests failed.  This can
make debugging pretty difficult.  To see this output, you must invoke tests
with

```sh
$ cd [build directory]
$ ctest --verbose
```

This will list which specific test from each test-case failed, and output all of
the diagnostic information necessary to debug the problem code.

# Writing Tests

If you have never written tests with Google Test or Google Mock before, read the
[primer] for Google Test and the "[For Dummies]" guide for Google Mock.

[pre-commit hook]: https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks
[primer]: https://github.com/google/googletest/blob/master/googletest/docs/Primer.md
[For Dummies]: https://github.com/google/googletest/blob/master/googlemock/docs/ForDummies.md
