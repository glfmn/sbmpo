#include "gtest/gtest.h"

#include "sample/distribution.h"

#include <limits>
#include <random>

using sample::distribution::Uniform;
using sample::distribution::UniformInt;

TEST(distribution_uniform, never_exceeds_bounds) {
    std::random_device rd; // only used once to initialise (seed) engine
    std::mt19937 rng(rd()); // random-number engine used
    std::uniform_int_distribution<int> minimum(1, 1000);
    std::uniform_real_distribution<double> value(0, 1);

    double eps = std::numeric_limits<double>::epsilon();

    for (size_t i = 0; i < 1000; ++i) {
        auto min = minimum(rng);
        std::uniform_int_distribution<int> maximum(min, min+1000);
        auto max = maximum(rng);

        auto dist = Uniform(double(min), double(max));

        EXPECT_TRUE(dist(1) - double(max) < eps)
            << "Max value from uniform distribution does not match its max\n"
            << "where:\n"
            << "    val: " << dist(0) << "\n"
            << "    min: " << double(min) << "\n"
            << "    max: " << double(max) << "\n";

        EXPECT_TRUE(dist(0) - double(min) < eps)
            << "Min value from uniform distribution does not match its min\n"
            << "where:\n"
            << "    val: " << dist(0) << "\n"
            << "    min: " << double(min) << "\n"
            << "    max: " << double(max) << "\n";

        auto v = dist(value(rng));
        ASSERT_TRUE(double(min) <= v && v <= double(max))
            << "Value " << v  << " falls outside of uniform distribution\n"
            << "where:\n"
            << "    min: " << double(min) << "\n"
            << "    max: " << double(max) << "\n";
    }
}

TEST(distribution_uniform_int, never_exceeds_bounds) {
    std::random_device rd; // only used once to initialise (seed) engine
    std::mt19937 rng(rd()); // random-number engine used
    std::uniform_int_distribution<int> minimum(1, 1000);
    std::uniform_real_distribution<double> value(0, 1);

    for (size_t i = 0; i < 1000; ++i) {
        auto min = minimum(rng);
        std::uniform_int_distribution<int> maximum(min, min+1000);
        auto max = maximum(rng);

        auto dist = UniformInt(min, max);

        EXPECT_TRUE(dist(1.0) == max)
            << "Max value in uniform int distribution does not match its max\n"
            << "where:\n"
            << "    val: " << dist(1) << "\n"
            << "    min: " << min << "\n"
            << "    max: " << max << "\n";

        EXPECT_TRUE(dist(0.0) == min)
            << "Min value in uniform int distribution does not match its min\n"
            << "where:\n"
            << "    val: " << dist(0) << "\n"
            << "    min: " << min << "\n"
            << "    max: " << max << "\n";

        auto v = dist(value(rng));
        ASSERT_TRUE(min <= v && v <= max)
            << "Value " << v  << " falls outside of uniform distribution\n"
            << "where:\n"
            << "    min: " << min << "\n"
            << "    max: " << max << "\n";
    }
}
