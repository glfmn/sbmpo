#include "gtest/gtest.h"

#include "sample/halton.h"

#include <limits>
#include <random>
#include <sstream>
#include <string>

auto simple_halton(unsigned index, unsigned base) -> double {
    unsigned i = index == 0 ? 1 : index;
    base = base < 2 ? 2 : base;
    double f = 1.;
    double r = 0.;
    while (i > 0) {
        f /= double(base);
        r = r + f * double(i % base);
        i = i/base;
    }

    return r;
}


TEST(halton, equals_known_base_2) {
    std::vector<double> base_2 = {
        1./2., 1./4., 3./4., 1./8., 5./8., 3./8., 7./8.,1./16., 9./16.
    };

    sample::HaltonSequence halton(2, 1);
    double eps = std::numeric_limits<double>::epsilon();

    for (auto b : base_2) {
        auto test = halton.advance();
        ASSERT_TRUE(abs(test - b) < eps)
            << "Generated value does not match known\n"
            << "  test:   " << test << "\n"
            << "  expect: " << b << "\n";
    }
}


TEST(halton, equals_known_base_3) {
    std::vector<double> base_3 = {
        1./3., 2./3., 1./9., 4./9., 7./9., 2./9., 5./9., 8./9., 1./27.
    };

    sample::HaltonSequence halton(3, 1);
    double eps = std::numeric_limits<double>::epsilon();

    for (auto b : base_3) {
        auto test = halton.advance();
        ASSERT_TRUE(abs(test - b) < eps)
            << "Generated value does not match known base 3\n"
            << "    test: " << test << "\n"
            << "  expect: " << b << "\n";
    }
}


TEST(halton, initial_value_matches_brute_force) {

    std::random_device rd; // only used once to initialise (seed) engine
    std::mt19937 rng(rd()); // random-number engine used
    std::uniform_int_distribution<int> index(1, 1000);
    std::uniform_int_distribution<int> base(2, 1000);

    double eps = std::numeric_limits<double>::epsilon();

    for (size_t i = 0; i < 1000; ++i) {
        i = index(rng);
        int b = base(rng);

        double bf = simple_halton(i, b);

        auto halton = sample::HaltonSequence(b, i);
        double test = halton.advance();

        ASSERT_TRUE(abs(test - bf) < 2*eps)
            << "First Halton does not match simple where (i,b): "
            << "(" << i << "," << b << ")" << "\n"
            << "      test: " << test << "\n"
            << "    simple: " << bf << "\n";
    }
}


TEST(halton, sequence_matches_brute_force) {

    std::random_device rd; // only used once to initialise (seed) engine
    std::mt19937 rng(rd()); // random-number engine used
    std::uniform_int_distribution<int> index(1, 1000);
    std::uniform_int_distribution<int> base(2, 1000);

    double eps = std::numeric_limits<double>::epsilon();

    for (size_t _i = 0; _i < 1000; ++_i) {
        int b = base(rng);

        unsigned i = index(rng);
        auto halton = sample::HaltonSequence(b, i);

        for (size_t _j = 0; _j < 100; ++_j) {
            double bf = simple_halton(i, b);
            double test = halton.advance();
            i++;

            ASSERT_TRUE(abs(test - bf) < 2*eps)
                << _j << ": HaltonSequence does not match simple where (i,b): "
                << "(" << i << "," << b << ")" << "\n"
                << "      test: " << test << "\n"
                << "    simple: " << bf << "\n";
        }
    }
}


TEST(halton, values_between_0_and_1) {

    std::random_device rd; // only used once to initialise (seed) engine
    std::mt19937 rng(rd()); // random-number engine used
    std::uniform_int_distribution<int> index(1, 1000);
    std::uniform_int_distribution<int> base(2, 1000);

    for (size_t _i = 0; _i < 1000; ++_i) {
        int b = base(rng);

        unsigned i = index(rng);
        auto halton = sample::HaltonSequence(b, i);

        for (size_t _j = 0; _j < 100; ++_j) {
            double test = halton.advance();

            ASSERT_TRUE(0 <= test && test < 1)
                << "Halton is greater than 1 where (i,b): "
                << "(" << i << "," << b << ")" << "\n"
                << "    " << test << " >= 1\n";
        }
    }
}

template<size_t N>
auto print(std::array<double, N> &a) -> std::string {
    std::stringstream result;
    result << "{" << a[0];

    for (size_t i = 1; i < a.size(); i++) {
        result << ", " << a[i];
    }
    result << "}";

    return result.str();
}

TEST(halton_generator, point_less_than_1) {

    std::random_device rd; // only used once to initialise (seed) engine
    std::mt19937 rng(rd()); // random-number engine used
    std::uniform_int_distribution<int> index(1, 1000);
    unsigned i = index(rng);

    // 100 dimensional halton sequence
    auto halton = sample::Halton<100>(i);

    for (size_t _i = 0; _i < 1000; ++_i) {

        auto point = halton.sample();

        for (auto p : point) {
            ASSERT_TRUE(p < 1) << "point: " << print(point) << "\n";
        }
    }
}
