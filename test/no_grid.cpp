#include "gtest/gtest.h"

#include <utility>

#include "grid/nogrid.h"

TEST(no_grid, insert_always_fails) {
    grid::NoGrid<int, int> grid;

    for (size_t i = 0; i < 100; ++i) {
        auto ok = grid.insert({0, 1}).second;
        ASSERT_FALSE(ok) << "insert must always fail";
    }
}


TEST(no_grid, self_blocks_insert) {
    grid::NoGrid<int, int> grid;

    for (size_t i = 0; i < 100; ++i) {
        std::pair<int,int> test = {100-i, i};
        auto block = grid.insert(test).first;
        ASSERT_EQ(block->first, test.first);
        ASSERT_EQ(block->second, test.second);
    }
}
