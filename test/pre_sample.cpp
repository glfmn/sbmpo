#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <limits>
#include <random>

#include "sample.h"
#include "sample/pre_sample.h"

using ::testing::Exactly;
using ::testing::Return;

namespace sample {
    template<size_t D>
    class MockGenerator : public Generator<D> {
    public:
        MOCK_METHOD0_T(sample, std::array<double, D>());
    };
}

TEST(pre_sample, cycle_samples_when_sample_call_count_exceeds_cached_size) {

    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<unsigned> count(1, 50);

    double eps = std::numeric_limits<double>::epsilon();

    for (size_t i = 0; i < 1000; ++i) {
        auto n = count(rng);

        // Create a set of 1D points of length n
        std::vector<std::array<double, 1>> points;
        points.reserve(n);
        for (int j = 0; j < n; ++j) {
            points.push_back({double(j)});
        }

        auto pre = sample::PreSample<1>(points);
        EXPECT_EQ(pre.size(), n)
            << "Size must match number of passed samples\n";

        sample::Generator<1> *gen = &pre;

        // Sample all of the samples
        for (unsigned j = 0; j < n; ++j) {
            gen->sample();
        }

        auto point = gen->sample();
        ASSERT_TRUE(point[0]-0 < eps);
    }
}

TEST(pre_sample, empty_returns_zero_vector) {
    double eps = std::numeric_limits<double>::epsilon();
    auto pre = sample::PreSample<1>();

    auto point = pre.sample();

    ASSERT_TRUE(point[0]-0 < eps);
}

TEST(pre_sample, create_from_generator_caches_right_number_of_samples) {
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<unsigned> count(0,50);

    std::array<double, 1> sample = {0};

    for (size_t test = 0; test < 1000; ++test) {
        auto n = count(rng);

        sample::MockGenerator<1> gen;
        EXPECT_CALL(gen,sample())
            .Times(Exactly(n))
            .WillRepeatedly(Return(sample));

        auto pre = sample::PreSample<1>(&gen, n);

        ASSERT_EQ(n, pre.size());
    }
}

TEST(pre_sample, size_respects_extension) {
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_int_distribution<unsigned> count(0,50);
    std::uniform_int_distribution<unsigned> extend_by(0,50);

    std::array<double, 3> sample = {0, 0, 0};

    for (size_t test = 0; test < 1000; ++test) {
        auto n = count(rng);
        auto e = extend_by(rng);

        sample::MockGenerator<3> gen;
        EXPECT_CALL(gen,sample())
            .Times(Exactly(n+e))
            .WillRepeatedly(Return(sample));

        auto pre = sample::PreSample<3>(&gen, n);
        pre.push_back(&gen, e);

        ASSERT_EQ(pre.size(), n + e) << "Extend initial " << n << " by " << e;
    }
}

TEST(pre_sample, returns_values_passed_in_order) {
    using Point = sample::PreSample<1>::Point;

    sample::MockGenerator<1> gen;
    EXPECT_CALL(gen,sample())
        .Times(Exactly(3))
        .WillOnce(Return(Point{1}))
        .WillOnce(Return(Point{2}))
        .WillOnce(Return(Point{3}));

    sample::PreSample<1> pre(&gen, 3);

    auto point = pre.sample();
    EXPECT_EQ(point[0], 1) << "Expected first passed value";

    point = pre.sample();
    EXPECT_EQ(point[0], 2) << "Expected second passed value";

    point = pre.sample();
    EXPECT_EQ(point[0], 3) << "Expected third passed value";

    point = pre.sample();
    ASSERT_EQ(point[0], 1) << "Expected wrap back to first value";
}

TEST(pre_sample, extend_does_not_change_current_element_after_cycle) {
    using Point = sample::PreSample<1>::Point;

    sample::MockGenerator<1> gen;
    EXPECT_CALL(gen,sample())
        .Times(Exactly(2))
        .WillOnce(Return(Point{1}))
        .WillOnce(Return(Point{2}));

    auto three = Point{3};
    auto four = Point{4};

    sample::PreSample<1> pre(&gen, 2);

    pre.sample();
    pre.sample();
    auto point = pre.sample();
    EXPECT_EQ(point[0], 1);

    pre.push_back(three);
    point = pre.sample();
    EXPECT_EQ(point[0], 2);

    point = pre.sample();
    EXPECT_EQ(point[0], 3);
}
