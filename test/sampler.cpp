#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include <array>

#include "sample.h"

using ::testing::Exactly;
using ::testing::Return;

struct Point3D {
    double x;
    double y;
    double z;

    Point3D(double x, double y, double z): x(x), y(y), z(z) {}
};

bool operator==(const Point3D &lhs, const Point3D &rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
}

namespace sample {

    template<>
    struct Sample<Point3D> {
    public:
        inline auto operator()(Generator<3> *g) -> Point3D {
            auto n = g->sample();
            return Point3D(n[0], n[1], n[2]);
        }
    };

    template<size_t D>
    class MockGenerator : public Generator<D> {
    public:
        MOCK_METHOD0_T(sample, std::array<double, D>());
    };

}  // namespace sampler

TEST(generator, generator_interface) {
    std::array<double, 3> expect = {0, 0, 0};

    sample::MockGenerator<3> gen;
    EXPECT_CALL(gen,sample())
        .Times(Exactly(1))
        .WillOnce(Return(expect));

    auto result = gen.sample();
    ASSERT_EQ(expect, result);
}

TEST(generator, sample_custom_type) {
    std::array<double, 3> sample = {0, 0, 0};

    sample::MockGenerator<3> gen;
    EXPECT_CALL(gen,sample())
        .Times(Exactly(1))
        .WillOnce(Return(sample));

    auto test = sample::Sample<Point3D>();

    auto result = test(&gen);
    ASSERT_EQ(Point3D(0, 0, 0), result);
}
